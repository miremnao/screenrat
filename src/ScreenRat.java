import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class ScreenRat
{
    private static void getScreenshot()
    {
        // getting current time
        Calendar calendar = new GregorianCalendar();
        DateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm.ss");
        timeFormat.setTimeZone(calendar.getTimeZone());
        String timeNow = timeFormat.format(calendar.getTime());

        //taking a screenshot
        try
        {
            BufferedImage screenshot = new Robot()
                    .createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit()
                            .getScreenSize()));
            ImageIO.write(screenshot, "JPG", new File("/home/miremnao/screens/" + timeNow + ".jpg"));
        }
        catch (Exception e)
        {
            System.err.print("Пиздарики :DDD");
        }
    }

    public static void main(String[] args)
    {
        getScreenshot();
    }
}